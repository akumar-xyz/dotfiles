# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
ZSH=/usr/share/oh-my-zsh/

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=13

# Plugins
plugins=(
  git
  globalias
  vi-mode
)


# User configuration

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' completions 1
zstyle ':completion:*' glob 1
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' max-errors 2 not-numeric
zstyle ':completion:*' substitute 1
zstyle ':completion:*' use-compctl true
zstyle :compinstall filename '/home/akumar/.zshrc'

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='nvim'
else
   export EDITOR='nvim'
fi

# Making vi-mode more tolerable 
export KEYTIMEOUT=0
## ci"
autoload -U select-quoted
zle -N select-quoted
for m in visual viopp; do
  for c in {a,i}{\',\",\`}; do
    bindkey -M $m $c select-quoted
  done
done

## ci{, ci(, di{ etc..
autoload -U select-bracketed
zle -N select-bracketed
for m in visual viopp; do
  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
    bindkey -M $m $c select-bracketed
  done
done

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias wget-rec='wget -mkEpnp -nH'
alias sctl='systemctl'
alias cd..='cd ..'
alias vim='nvim'
alias vi='nvim'
alias sudo='doas'
alias bc='bc -l'
alias weather='curl wttr.in'
alias grep='grep --color=auto'
alias rmrf='rm -rf'
alias cls='clear'
alias bc='eva'
alias sync-notes='rclone bisync /home/akumar/Sync/cryptdav  cryptdav:sync/  -i'
alias mpa='mpv --no-vid ' # MPA Plays Audios
alias tdu='du -chs'
# alias man='w3mman'

alias cdp='cd ~/Programming/'
alias cdn='cd ~/MyNotes/'
alias cdmo='cd ~/Programming/mindo-tech'

# System Update
alias nothingtoseehere='reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist && powerpill -Syuw'
alias justfuckmyshitupfam='sudo pacman -Syyu && yay -Suya'

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh

# Bind Alr+\ to ncmpcpp
ncmpcppShow() { ncmpcpp <$TTY; zle redisplay; }
zle -N ncmpcppShow
bindkey '^[\' ncmpcppShow

# Bind ctrl+\ to lf
lfShow() { lf <$TTY; zle redisplay; }
zle -N lfShow
bindkey '^\' lfShow

# Bind ctrl+N to newsboat
newsboatShow() { newsboat <$TTY; zle redisplay; }
zle -N newsboatShow
bindkey '^n' newsboatShow

# Bind ctrl-O to notes
notesShow() { 
    pushd "$HOME/MyNotes"
    nvim <$TTY;
    zle redisplay; 
    popd
}
zle -N notesShow
bindkey '^o' notesShow

# Bind alt+o to xdg-open anything
function fzfOpen() { 
	local toopen=$(find . -type f | fzf);
	if [ -z $toopen ]
	then
		echo $toopen
		echo "Didn't open anything!'"
	else
		print $toopen
		mimeopen -n "$toopen" & disown
	fi
	zle redisplay; 
}
zle -N fzfOpen 
bindkey '\eo'  fzfOpen


# mpv youtube song search
function mm() {
    mpv --no-video --ytdl-format=bestaudio ytdl://ytsearch:"$@"
}

# Search youtube music and play
function ytm() {
    mpv --ytdl-format=bestaudio "https://music.youtube.com/search?q=${@// /+}"
}

# mkcd
function mkcd() {
        mkdir -p $*
        cd $*
}

# Use lf to pick file
function lfpickfile() {
  item=`echo -n \"${$(lfpick)}\"`
  LBUFFER="${LBUFFER}$item"
}
zle     -N   lfpickfile
bindkey '\el' lfpickfile

# wordless
wordless(){
	unzip -p "$1" word/document.xml | sed -e 's|</w:p>|\n|g; s/<[^>]\+>//g; s/[^[:print:]\n]//g' | less
}

# Bind ctrl+\ to nvim
nvimStart() { nvim <$TTY; zle redisplay; }
zle -N nvimStart
bindkey '^p' nvimStart

# # Bash insulter
# source /etc/bash.command-not-found

# Fish-like syntax hilighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Enable powerlevel9k
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# Fzf bindings
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Import icons for lf
[ -f ~/.lf_icons.zsh ] && source ~/.lf_icons.zsh

# Path for script files
export PATH=$PATH:$HOME/.scripts

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
