#!/usr/bin/sh

super="Mod4"
alt="Mod1"

# Launch terminal
riverctl map normal $super Return spawn footclient

# Application Launcher
riverctl map normal $alt Return spawn "wofi --show drun"

# Brightness Control
riverctl map normal None XF86MonBrightnessUp spawn 'brightnessctl set 5%+'
riverctl map normal None XF86MonBrightnessDown spawn 'brightnessctl set 5%-'
riverctl map normal $super XF86MonBrightnessDown spawn 'brightnessctl set 0'

# Volume
riverctl map normal None XF86AudioLowerVolume spawn 'pactl set-sink-volume 0 -5%'
riverctl map normal None XF86AudioMute spawn 'pactl set-sink-mute 0 toggle'
riverctl map normal None XF86AudioRaiseVolume  spawn 'pactl set-sink-volume 0 +5%'
riverctl map normal None XF86AudioMicMute  spawn 'pactl set-source-mute 0 toggle'

riverctl map normal None XF86AudioPlay spawn 'mpc toggle'
riverctl map normal None XF86AudioStop spawn 'mpc stop'
riverctl map normal None XF86AudioPrev spawn 'mpc prev'
riverctl map normal None XF86AudioNext spawn 'mpc next'

riverctl map normal $super+Control Space spawn "makoctl dismiss" # Dismiss Notification
riverctl map normal $super P spawn '$HOME/.scripts/dmenu_shutdown' # Shutdown Menu
riverctl map normal $super E spawn 'plumb' # Plumbing script
riverctl map normal $super B spawn 'clipman pick -t wofi' # Clipboard Manager

# Insert emoji with super+.
riverctl map normal $super COMMA spawn 'dmenu_emoji_insert.sh'

# Password Management
riverctl declare-mode passwords
riverctl map normal $super+Shift P enter-mode passwords
riverctl map passwords $super+Shift P enter-mode normal
riverctl map passwords $super Escape enter-mode normal

riverctl map passwords $super P spawn 'wl-passmenu ; riverctl enter-mode normal'
riverctl map passwords $super U spawn 'wl-passmenu -m username ; riverctl enter-mode normal'
riverctl map passwords $super O spawn 'wl-passmenu -m totp ; riverctl enter-mode normal'
riverctl map passwords $super E spawn 'wl-passmenu -m entry ; riverctl enter-mode normal'

# Screenshot
riverctl map normal $super+Shift S spawn 'grimshot copy area'
riverctl map normal $super S spawn 'grimshot save area ~/Screenshots/$(date +%s).png'

# OCR
riverctl map normal $super+Shift O spawn 'ocr.sh'

# Super Paste (for annoying websites that disable pasting)
riverctl map normal $super+Control V spawn '$HOME/.scripts/tpaste'

# Bring up logseq
LOGSEQ_TAG=$((1 << 21))
riverctl map normal $super F12 toggle-focused-tags ${LOGSEQ_TAG}
riverctl map normal $super+Shift F12 set-view-tags ${LOGSEQ_TAG}
# Set spawn tagmask to ensure new windows don't have the scratchpad tag unless
# explicitly set.
all_but_scratch_tag=$(( ((1 << 32) - 1) ^ $LOGSEQ_TAG ))
riverctl spawn-tagmask ${all_but_scratch_tag}
