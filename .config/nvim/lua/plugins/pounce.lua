return {
  'rlane/pounce.nvim',
  keys = {
    {'s', function() require('pounce').pounce {} end, 'n'},
    {'S', function() require('pounce').pounce { do_repeat = true } end, 'n'},
    {'s', function() require('pounce').pounce {} end, 'x'},
    {'gs', function() require('pounce').pounce {} end, 'o'},
    {'S', function() require('pounce').pounce { input = { reg = '/' } } end, 'n'},
  }
}
