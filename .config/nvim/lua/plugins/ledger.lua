return {
  {
    -- General configuration
    'ledger/vim-ledger',
    ft= "ledger",
  },
  {
    -- Completion source
    'kirasok/cmp-hledger',
    ft= "ledger",
  },
}

