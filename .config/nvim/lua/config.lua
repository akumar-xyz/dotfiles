-- Set to true if you have a Nerd Font installed and selected in the terminal
vim.g.have_nerd_font = false

-- [[ Setting options ]]
-- See `:help vim.opt`
-- For more options, you can see `:help option-list`

-- Case-insensitive searching UNLESS \C or one or more capital letters in the search term
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Show which line your cursor is on
vim.opt.cursorline = true

-- Make line numbers default
vim.opt.relativenumber = true
vim.opt.number = true

-- Configure how new splits should be opened
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Enable mouse mode, can be useful for resizing splits for example!
vim.opt.mouse = 'a'

-- Don't show the mode, since it's already in the status line
vim.opt.showmode = false
vim.opt.breakindent = true

-- Sets how neovim will display certain whitespace characters in the editor.
--  See `:help 'list'`
--  and `:help 'listchars'`
-- vim.opt.list = true
-- vim.opt.listchars = { tab = '» ', trail = '·', nbsp = '␣' }

vim.opt.signcolumn = 'yes'
vim.opt.wrap = false -- do not wrap

-- Preview substitutions live, as you type!
vim.opt.inccommand = 'split'

-- [[ Basic Autocommands ]]
--  See `:help lua-guide-autocommands`

-- Highlight when yanking (copying) text
--  Try it with `yap` in normal mode
--  See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd('TextYankPost', {
  pattern = '*',
  callback = function(ev)
    vim.highlight.on_yank { higroup = 'Visual', timeout = 300 }
  end,
})

-- netrw config
vim.g.netrw_banner = 1 -- Hide banner
vim.g.netrw_browse_split = 4 -- Open in previous window
vim.g.netrw_altv = 1 -- Open with right splitting
vim.g.netrw_liststyle = 3 -- Tree-style view
vim.g.netrw_list_hide = (vim.fn['netrw_gitignore#Hide']()) .. [[,\(^\|\s\s\)\zs\.\S\+]] -- use .gitignore
vim.g.netrw_winsize = 25
vim.g.netrw_bufsettings = 'noma nomod nu nowrap ro nobl'
vim.g.netrw_fastbrowse = 0
vim.g.netrw_sizestyle = 'H'
vim.g.netrw_usetab = true

-- Open files relative to open buffer when using 'gx'
-- vim.g.netrw_browsex_viewer = "xdg-open"
vim.g.netrw_browsex_viewer = 'nvim-open %:h'

-- Fold text
vim.wo.foldtext = [[substitute(getline(v:foldstart),'\\t',repeat('\ ',&tabstop),'g').'…'.. ' (' . (v:foldend - v:foldstart + 1) . ' lines)']]

-- [[ Install `lazy.nvim` plugin manager if not installed ]]
--    See `:help lazy.nvim.txt` or https://github.com/folke/lazy.nvim for more info
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.uv.fs_stat(lazypath) then
  local lazyrepo = 'https://github.com/folke/lazy.nvim.git'
  local out = vim.fn.system { 'git', 'clone', '--filter=blob:none', '--branch=stable', lazyrepo, lazypath }
  if vim.v.shell_error ~= 0 then
    error('Error cloning lazy.nvim:\n' .. out)
  end
end ---@diagnostic disable-next-line: undefined-field
vim.opt.rtp:prepend(lazypath)

-- .hledger files are ledger filetypes
vim.filetype.add({
  extension = {
    hledger = 'ledger',
  }
})
