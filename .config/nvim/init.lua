require 'config'
require 'keybinds'

-- Lazy configurations (Plugins)
require('lazy').setup({
  -- Load  plugin configurations from lua/plugins
  { import = 'plugins' },

  -- Configure other plugins
  'tpope/vim-sleuth', -- Detect tabstop and shiftwidth automatically
}, {
  ui = {
    -- If you are using a Nerd Font: set icons to an empty table which will use the
    -- default lazy.nvim defined Nerd Font icons, otherwise define a unicode icons table
    icons = vim.g.have_nerd_font and {} or {
      cmd = '⌘',
      config = '🛠',
      event = '📅',
      ft = '📂',
      init = '⚙',
      keys = '🗝',
      plugin = '🔌',
      runtime = '💻',
      require = '🌙',
      source = '📄',
      start = '🚀',
      task = '📌',
      lazy = '💤 ',
    },
  },
})

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
