Use `./.deploy.sh` to deploy the dotfiles
Use `./.clean_up.sh` to clean up the stowed directories

# Example To add another folder to the .dotfile foler

```shell
mv $HOME/.config/app $HOME/.dotfiles/.config
~/.dotfiles/.clean_up.sh
~/.dotfiles/.deploy.sh
```
