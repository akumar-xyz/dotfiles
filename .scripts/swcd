#!/bin/sh

wallpapers_home="$HOME/Pictures/Wallpapers"
sleep_time="20m"

daemon_name="SimpleWallpaperChangerDaemon"


pid_file="/tmp/$USER.$daemon_name.pid"

core()
{
    curr_bg=$(shuf -n1 -e "$wallpapers_home"/*)
    killall -9 swaybg
    swaybg -i "$curr_bg" &
}

myPid=`echo $$`

start_daemon()
{
    # Start the daemon.
    if daemon_is_running ; then
        echo "$daemon_name is already running."
        exit 1
    fi
    log "* Starting $daemon_name with PID: $myPid."
    echo "$myPid" > "$pid_file"
    log " Starting up $daemon_name."

  # Start the loop.
  loop
}

stop_daemon()
{
    # Stop the daemon.
    if ! daemon_is_running ; then
        log "* $daemon_name not running."
        exit 1
    fi
    log "* Stopping $daemon_name"
    log "* $daemon_name stopped."

    if [[ ! -z `cat $pid_file` ]]; then
        kill -9 `cat "$pid_file"` &> /dev/null
    fi
    cleanup
}

status_daemon()
{
    # Query and return whether the daemon is running.
    if  daemon_is_running ; then
        echo " * $daemon_name is running."
    else
        echo " * $daemon_name isn't running."
    fi
    exit 0
}

next_bg()
{
    # Restart the daemon.
    if ! daemon_is_running ; then
        # Can't restart it if it isn't running.
        echo "$daemon_name isn't running."
        exit 1
    fi
    core
}

daemon_is_running()
{
    # Check to see if the daemon is running.
    # This is a different function than status_daemon
    # so that we can use it other functions.
    if [ -f "$pid_file" ]; then
        # Check if the PID is valid
        read pid <$pid_file
        echo "PID of Running daemon : $pid"
        # Daemon is running.
        return 0
    fi
    # Daemon isn't running.
    return 1
}

loop()
{
    while [[ true ]]; do
        core
        sleep "$sleep_time"
    done
}

log()
{
    echo "*** $(date --rfc-3339=seconds): $1"
}

print_help()
{
    echo "usage $1 { start | stop | restart | status }"
    echo ""
}

cleanup ()
{
    rm "$pid_file"
}

cleanup_and_exit ()
{
    cleanup
    exit 0
}

################################################################################
# initialise trap to call trap_ctrlc function
# when signal 2 (SIGINT) is received
################################################################################

trap cleanup_and_exit 2

################################################################################
# Parse the command.
################################################################################

case "$1" in
    start)
        start_daemon
        ;;
    stop)
        stop_daemon
        ;;
    status)
        status_daemon
        ;;
    next)
        next_bg
        ;;
    *)
        print_help "$0"
        exit 1
esac

