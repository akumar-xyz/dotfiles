#!/usr/bin/sh

tmpfile=$(mktemp --suffix=.png)
grimshot save area $tmpfile
tesseract $tmpfile - | wl-copy
rm $tmpfile
notify-send "OCR Job Completed"
