#!/usr/bin/sh

REPL_INIT_COMMANDS="
	repl.interface = REPL.setup_interface(repl)
	repl.interface.modes[1].prompt = \"=> \"
	print(\"\033c\")
	run(\`figlet 'Oy vey... calculator!' \`)
"
CALINIT="import REPL 
function myrepl(repl)
$REPL_INIT_COMMANDS
return
end
atreplinit(myrepl)"

julia -i --banner=no -e "$CALINIT"

