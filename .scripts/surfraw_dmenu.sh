#!/usr/bin/sh

service=$(surfraw -elvi | sed -e '1d; s/--/\t\t/g' | fzf | awk '{print $1}')

read -r searchterm
echo $service "$searchterm"
sr $service $searchterm & disown

