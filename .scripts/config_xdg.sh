#!/usr/bin/env sh

temp_file=$(mktemp)
echo "# Only one application found for the following MIME(s)"  >> "$temp_file"

get_all_mime() {
    cat /usr/share/applications/*.desktop |\
        grep "MimeType" |\
        sed -e '/Entry\]/d' -e 's/^.*MimeType=//g' -e 's/;/\n/g' |\
        sort -b -u
    }

for mime_type in $(get_all_mime); do
    mime_applications=$(grep -l "$mime_type" /usr/share/applications/*.desktop)
    app_count=$(echo "$mime_applications" | wc -l)
    if [[ $app_count -eq 1 ]]; then
        app_name=$(basename $mime_applications)
        echo "xdg-mime default $app_name $mime_type " >> "$temp_file"
    else
        printf "#$mime_type\n"
        for i in $mime_applications; do
            app_name=$(basename "$i")
            printf "xdg-mime default $app_name $mime_type \n"
        done
        printf "\n\n"
    fi 
done

cat "$temp_file"
rm "$temp_file"
