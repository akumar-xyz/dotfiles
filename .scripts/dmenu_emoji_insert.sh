#!/bin/sh

# The famous "get a menu of emojis to copy" script.

# Get user selection via dmenu from emoji file.
chosen=$(awk -F ' ; ' '{print  $2, $3 }' < "$HOME/.local/share/emoji" | wofi --dmenu -i -p "Select Emoji" | sed 's/ .*//')

# Exit if none chosen.
[ -z "$chosen" ] && exit

# wtype the chosen option
wtype "$chosen"
