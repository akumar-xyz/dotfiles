#!/bin/bash
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
dir=$HOME/.dotfiles                    # dotfiles directory

# change to the dotfiles directory
echo "Changing to the $dir directory... "
cd "$dir" || exit 1 && echo "...done"

echo "Performing stow magic... "
stow . -t "$HOME" && echo "...done"

echo "setting up emojis file"
"$HOME/.scripts/get_emoji_list.sh" > "$HOME/.local/share/emoji" && echo "...done"

